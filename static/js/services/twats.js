﻿app.service('twatService', function($http, $q) {
	this.backend = 'http://192.168.153.86:5555/api';
	this.getTwats = function(token) {
		var result = $q.defer();
		$http.get(this.backend + '/twats/getUserFeed.json?token=' + token).success(function(data) {
			result.resolve(data);
		});
		return result.promise;
	};
	this.getUserTwats = function(id) {
		var result = $q.defer();
		$http.get(this.backend + '/twats/getUsersTwats.json?userid=' + id).success(function(data) {
			result.resolve(data);
		});
		return result.promise;
	};
	this.postTwat = function(token, content) {
		if(content === '') return false;
		var result = $q.defer();
		$http.post(this.backend + '/twats/post.json', {token: token, content: content}).success(function(data) {
			result.resolve(data);
		});
		return result.promise;
	};
	this.deleteTwat = function(token, id) {
		var result = $q.defer();
		$http.post(this.backend + '/twats/deleteTwat.json', {token: token, id: id}).success(function(data) {
			result.resolve(data);
		});
		return result.promise;
	};
	this.retwat = function(token, id) {
		return this.postTwat(token, '{retwat@' + id + '}');
	};
	this.parseTwat = function(twat) {
		twat = twat.replace(/(§[^§\s\W]+)/g, '<a href="#/search/$1">$1</a>');
		twat = twat.replace(/(@[^§\s\W]+)/g, '<a href="#/search/$1">$1</a>');
		return twat;
	};
	this.toggleLove = function(id, token) {
		var result = $q.defer();site
		$http.post(this.backend + '/twats/love.json', {token: token, id: id}).success(function(data) {
			result.resolve(data);
		});
		return result.promise;
	};
});
