app.service('userService', function($http, $q) {
	this.backend = 'http://192.168.153.86:5555/api';
	this.getUserInfo = function(token) {
		var result = $q.defer();
		$http.get(this.backend + '/user/infoByToken.json?token=' + token).success(function(data) {
			result.resolve(data);
		});
		return result.promise;
	};
	this.getUserInfoById = function(id) {
		var result = $q.defer();
		$http.get(this.backend + '/user/infoById.json?id=' + id).success(function(data) {
			result.resolve(data);
		});
		return result.promise;
	};

	this.getUserTwats = function(id) {
		var result = $q.defer();
		$http.get(this.backend + '/twats/getUsersTwats.json?userid=' + id).success(function(data) {
			result.resolve(data);
		});

		return result.promise;
	};

	this.getUserStats = function(id) {
		var result = $q.defer();
		$http.get(this.backend + '/getUserStats.json?userid=' + id).success(function(data) {
			result.resolve(data);
		});

		return result.promise;
	};

	this.getFollowing = function(id) {
		var result = $q.defer();
		$http.get(this.backend + '/getFollowing.json?id=' + id).success(function(data) {
			result.resolve(data);
		});

		return result.promise;
	};

	this.getFollowers = function(id) {
		var result = $q.defer();
		$http.get(this.backend + '/getFollowers.json?id=' + id).success(function(data) {
			result.resolve(data);
		});
		return result.promise;
	}
});
