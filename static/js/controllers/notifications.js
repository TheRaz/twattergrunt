app.controller('notifications', function($scope, $http, $cookies, $routeParams) {
	var verifyToken = function() {
		if($cookies.token !== '') {
			$http.post($scope.backend + '/verifyToken.json', {token: $cookies.token}).
			success(function(data) {
				if(data.status !== true) {
					$location.path('/login');
					return false;
				}
				return true;
			}).
			error(function(data) {
				$location.path('/login');
				return false;
			});
		}
	};
	verifyToken();
	$scope.query = $routeParams.query;
  $http.get($scope.backend + '/notifications/getall.json?token=' + $cookies.token).success(function(res) {
		$scope.results = res;
	});
	$http.post($scope.backend + '/notifications/markasread.json', {token: $cookies.token}).success(function(res) {

	});
});
