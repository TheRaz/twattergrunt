app.controller('dashboard', function($q, $scope, $http, $location, $cookies, userService, twatService) {
	$scope.users = [];
	var defer = $q.defer();
	$scope.userPromises = [];
	var verifyToken = function() {
		if($cookies.token !== '') {
			$http.post($scope.backend + '/verifyToken.json', {token: $cookies.token}).
			success(function(data) {
				if(data.status !== true) {
					$location.path('/login');
					return false;
				}
				return true;
			}).
			error(function(data) {
				$location.path('/login');
				return false;
			});
		}
	};
	verifyToken();

	userService.getUserInfo($cookies.token).then(function(user) {
		$scope.user = user;
		$scope.users[user.id] = user;

		userService.getUserStats($scope.user.id).then(function(stats) {
				$scope.stats = stats;
		});
		twatService.getTwats($cookies.token).then(function(twats) {
			$scope.twats = twats;
		});
	});
});
