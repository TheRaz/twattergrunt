app.controller('logout', function($scope, $http, $location, $cookies) {
	$scope.logout = function() {
		$http.post($scope.backend + '/logout.json', {token: $cookies.token}).
		success(function(data) {
				$cookies.token = undefined;
				$location.path('/login');
		}).
		error(function(data) {
				$cookies.token = undefined;
				$location.path('/login');
		});
	};
	$scope.logout();
});
