app.controller('site', function($scope, $rootScope, $http, $location, $cookies, $route, $interval, userService, twatService) {
  $scope.controller = 'dashboard';
  $scope.backend = 'http://192.168.153.86:5555/api';
  $scope.unreadNotifications = 0;
  var getNotifications = function() {
    $http.get($scope.backend + '/notifications/new.json?token=' + $cookies.token).success(function(res) {
      $scope.unreadNotifications = res.message;
    });
  };

  getNotifications();
  $interval(function () {
    getNotifications();
  }, 15000);

  $rootScope.$on("$routeChangeSuccess", function(e, data) {
    $scope.controller = data.controller;
    getNotifications();
    userService.getUserInfo($cookies.token).then(function(result) {
      $scope.navuser = result;
    });
  });
  $scope.search = function(query) {
    console.log($scope.searchfield);
    $location.path('/search/' + query);
  };
  $scope.parseTwat = function(twat) {
    return twatService.parseTwat(twat);
  };
  $scope.postTwat = function(twat) {
   if(twat.length > 141){
    swal({
     title: "yo gots to many letterz dawg",
     text: "you better recognize",
     timer: 4000,
      showConfirmButton: false });
    return;
   }
    twatService.postTwat($cookies.token, twat).then(function(data) {
      if(data.status == true) {
        $route.reload();
      }

    });
  };
  $scope.deleteTwat = function(id) {
    twatService.deleteTwat($cookies.token, id).then(function(data){
      $route.reload();

    });
  };
  $scope.retwat = function(id) {
    twatService.retwat($cookies.token, id).then(function(data){
      $route.reload();
    });
  };
  $scope.respond = function(username) {
    $scope.twat = "@" + username + " ";
    document.querySelector(".postbox").focus();
  };
 $scope.pretty = function(time){
   var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
   diff = (((new Date()).getTime() - date.getTime()) / 1000),
   day_diff = Math.floor(diff / 86400);

   if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
   return;

   return day_diff == 0 && (
    diff < 60 && "just now" ||
    diff < 120 && "1 minute ago" ||
    diff < 3600 && Math.floor( diff / 60 ) + " min" ||
    diff < 7200 && "1 hour ago" ||
    diff < 86400 && Math.floor( diff / 3600 ) + " tim") ||
    day_diff == 1 && "Yesterday" ||
    day_diff < 7 && day_diff + " days ago" ||
    day_diff < 31 && Math.ceil( day_diff / 7 ) + " weeks ago";

  };
});
