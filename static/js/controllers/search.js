app.controller('search', function($scope, $http, $location, $routeParams, $cookies) {
	var verifyToken = function() {
		if($cookies.token !== '') {
			$http.post($scope.backend + '/verifyToken.json', {token: $cookies.token}).
			success(function(data) {
				if(data.status !== true) {
					$location.path('/login');
					return false;
				}
				return true;
			}).
			error(function(data) {
				$location.path('/login');
				return false;
			});
		}
	};
	verifyToken();
	$scope.query = $routeParams.query;
  $http.post($scope.backend + '/search.json', {query: $scope.query}).success(function(res) {
		$scope.results = res;
	});
});
