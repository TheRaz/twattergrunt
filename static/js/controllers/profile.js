app.controller('profile', function($scope, $http, $location, $cookies, $routeParams, userService, twatService) {
	$scope.following = false;
	var verifyToken = function() {
		if($cookies.token !== '') {
			$http.post($scope.backend + '/verifyToken.json', {token: $cookies.token}).
			success(function(data) {
				if(data.status === true) {
					return true;
				}
			}).
			error(function(data) {
				$location.path('/login');
				return false;
			});
		}
	};
	verifyToken();
	var checkFollowing = function() {
		if($scope.me.id != $scope.user.id) {
			$http.get($scope.backend + '/follow/isFollowing.json?token=' + $cookies.token + '&target=' + $scope.user.id).success(function(res) {
				$scope.following = res.status;
			});
		}
	}
	$scope.toggleFollow = function() {
		$http.post($scope.backend + '/follow/followUnfollow.json', {token: $cookies.token, target: $scope.user.id}).success(function(res) {
			$scope.following = res.status;
		});
	};

	userService.getUserInfo($cookies.token).then(function(user) {
		$scope.me = user;
		var id = ($routeParams.id > 0) ? $routeParams.id : user.id;

		userService.getUserInfoById(id).then(function(user) {
			$scope.user = user;
			userService.getUserStats($scope.user.id).then(function(stats) {
					$scope.stats = stats;
			});
			if($scope.user.id != $scope.me.id) {
				$scope.twat = '@' + $scope.user.username + ' ';
			}
			twatService.getUserTwats(id).then(function(twats) {
				$scope.twats = twats;
			});
				userService.getFollowing(id).then(function(following) {
					$scope.following = following;
				});
				userService.getFollowers(id).then(function(followers) {
					$scope.followers = followers;
				});
			checkFollowing();
		});


	});
});
