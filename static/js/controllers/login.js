app.controller('login', function($scope, $http, $location, $cookies) {
	var verifyToken = function() {
		if($cookies.token !== '') {
			$http.post($scope.backend + '/verifyToken.json', {token: $cookies.token}).
			success(function(data) {
				if(data.status === true) {
					$location.path('/dashboard');
					return true;
				}
				return false;
			}).
			error(function(data) {
				return false;
			});
		}
	};
	verifyToken();
	$scope.login = function() {
		$http.post($scope.backend + '/login.json', {username: $scope.username, password: $scope.password}).
		success(function(data, status, headers, config) {
			console.log(data);
			if(data.loggedIn === true && data.token !== '') {
				$cookies.token = data.token;
				verifyToken();
			}else {
				swal("Fel information", "Säker på att du fyllde i rätt användarnamn & lösenord?", "error");
			}
		}).
		error(function(data, status, headers, config) {
			swal("Något gick fel", "Försök igen!", "error");
		});
	};
	$scope.register = function() {
		$http.post($scope.backend + '/register.json', {firstname: $scope.regfirstname, lastname: $scope.reglastname, username: $scope.regusername, password: $scope.regpassword, email: $scope.regemail}).
		success(function(data) {
				if(data.status === false) {
					swal("User Exists", "try another username", "error");
				}else {
					swal({
						title: "Grattis!",
						text: "Du har nu skapat ett konto på Twatter!",
						type: "success"
					},function(){
						$("#registerpanel").slideUp();
					});
				}
		});

	}
});
