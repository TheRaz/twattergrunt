app = angular.module('twatter', ['ngRoute','ngCookies']);
app.config(function($routeProvider, $httpProvider) {

	$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

	var param = function(obj) {
		var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

		for(name in obj) {
			value = obj[name];

			if(value instanceof Array) {
				for(i=0; i<value.length; ++i) {
					subValue = value[i];
					fullSubName = name + '[' + i + ']';
					innerObj = {};
					innerObj[fullSubName] = subValue;
					query += param(innerObj) + '&';
				}
			}
			else if(value instanceof Object) {
				for(subName in value) {
					subValue = value[subName];
					fullSubName = name + '[' + subName + ']';
					innerObj = {};
					innerObj[fullSubName] = subValue;
					query += param(innerObj) + '&';
				}
			}
			else if(value !== undefined && value !== null)
				query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
		}

		return query.length ? query.substr(0, query.length - 1) : query;
	};

	$httpProvider.defaults.transformRequest = [function(data) {
		return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
	}];


	$routeProvider
	.when('/dashboard', {
		templateUrl: 'static/js/views/dashboard.html',
		controller: 'dashboard'
	})
	.when('/profile/:id', {
		templateUrl: 'static/js/views/profile.html',
		controller: 'profile'
	})
	.when('/login', {
		templateUrl: 'static/js/views/login.html',
		controller: 'login'
	})
	.when('/logout', {
		templateUrl: 'static/js/views/login.html',
		controller: 'logout'
	})
	.when('/notifications', {
		templateUrl: 'static/js/views/notifications.html',
		controller: 'notifications'
	})
	.when('/search/:query', {
		templateUrl: 'static/js/views/search.html',
		controller: 'search'
	})
	.otherwise({
		redirectTo: '/login'
	});
});
app.filter('to_trusted', ['$sce', function($sce){
		return function(text) {
				return $sce.trustAsHtml(text);
		};
}]);
